<?php

abstract class Api {

    protected $request;
    protected $response;

    public function __construct($request) {
        require_once API_BASEPATH . '/classes/Api_Response.php';
        $this->request = $request;
        $this->response = new Api_Response($request);
    }

    public function build_response() {
        switch (true) {
            case !isset($this->request[2]):
                return (array) $this->default_response();
            case method_exists($this, $this->request[2] . '_action'):
                $method = $this->request[2] . '_action';
                $args = array_slice($this->request, 3);
                return call_user_func_array(array($this, $method), $args);
            default:
                return (array) $this->error_response();
        }
    }

    private function error_response() {
        return array('error' => 'No method exists');
    }

    abstract function default_response();
}