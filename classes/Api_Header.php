<?php

require_once API_BASEPATH . '/classes/Api.php';

class Api_Header extends Api {

    /**
     * 
     * @return type
     */
    public function default_response() {

        $data = array(
            'menus' => array(),
            'blog_options' => $this->blog_options_action(),
            'tableau_servers_white_list' => get_option('tableau_servers_white_list')
        );

        $menu_ids = get_nav_menu_locations();

        foreach ($menu_ids as $menu_location => $menu_id) {
            if ($menu_id > 0) {
                $data['menus'][$menu_location] = $this->_get_menu($menu_id);
            }
        }

        return $this->response->response($data);
    }

    /**
     * 
     * @param type $menu_location
     * @return type
     */
    public function menu_action($menu_location = null) {
        $data = array(
            'menus' => array(),
        );
        $locations = get_nav_menu_locations();
        if ($menu_location && isset($locations[$menu_location])) {
            $menu_id = $locations[$menu_location];
        } else {
            $default_menu = array_slice($locations, 0, 1);
            $default_menu_keys = array_keys($default_menu);
            $menu_location = $default_menu_keys[0];
            $menu_id = $default_menu[$menu_location];
        }
        $data['menus'][$menu_location] = $this->_get_menu($menu_id);
        return $this->response->response($data);
    }

    /**
     * 
     * @param type $what
     * @return type
     */
    public function recompile_action($what = null) {
        if (current_user_can('administrator') && in_array($what, array('css', 'js'))) {
            $file = get_template_directory() . "/$what/main.min.$what";
            if (file_exists($file) && unlink($file)) {
                return $this->response->response(array('msg' => "$what file has been recompiled"), false);
            }
        }
        $this->response->send_404();
    }

    /**
     * 
     * @return type
     */
    public function blog_options_action() {
        $options = get_option('vdh_theme_options');
        if ($options && isset($options['logo']) && preg_match('/^\//', $options['logo'])) {
            $options['logo'] = get_bloginfo('url') . $options['logo'];
        }
        if (isset($options['favicon'])) {
            unset($options['favicon']);
        }
        $options['name'] = get_bloginfo('name');
        $options['description'] = get_bloginfo('description');
        return $options;
    }

    /**
     * 
     * @param type $menu_id
     * @return type
     */
    private function _get_menu($menu_id) {

        $menu = wp_get_nav_menu_object($menu_id);
        $menu_items = wp_get_nav_menu_items($menu->term_id, array('update_post_term_cache' => false));
        
        $sorted_items = array();
        foreach ((array) $menu_items as $menu_item) {
            $sorted_items[] = $this->_toArray($menu_item);
        }
        $menu_tree_items = $this->_get_children($sorted_items, $sorted_items);
        foreach ($menu_tree_items as $key => $menu_tree_item) {
            if ($menu_tree_item['parent'] !== '0') {
                unset($menu_tree_items[$key]);
            }
        }
        return array_values((array) $menu_tree_items);
    }

    /**
     * 
     * @param type $elements
     * @param type $items
     * @return type
     */
    private function _get_children($elements, $items) {
        foreach ($elements as $key => $element) {
            $children = array();
            foreach ($items as $item) {
                if ($item['parent'] == $element['id']) {
                    $children[] = $item;
                }
            }
            $elements[$key]['children'] = $this->_get_children($children, $items);
        }
        return $elements;
    }

    /**
     * 
     * @param type $menu_item
     * @return array
     */
    private function _toArray($menu_item) {
        $item = array(
            'id' => $menu_item->ID,
            'name' => $menu_item->title,
            'url' => $menu_item->url,
            'parent' => $menu_item->menu_item_parent,
            'children' => array()
        );
        return $item;
    }

}
