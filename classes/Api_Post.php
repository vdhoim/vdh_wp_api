<?php

require_once API_BASEPATH . '/classes/Api.php';

class Api_Post extends Api {

    public function default_response() {
        return $this->url_action();
    }

    public function url_action() {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
        $response = get_post($id, ARRAY_A);
        if ($response) {
            $response['permalink'] = get_permalink($id);
        } else {
            $response = array('result' => false, 'msg' => 'post not found');
        }
        return $this->response->response($response);
    }
}