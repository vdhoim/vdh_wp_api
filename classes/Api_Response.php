<?php

class Api_Response {

    protected $request;

    public function __construct($request) {
        $this->request = $request;
    }

    public function response($data, $include_endpoint = true) {
        if($include_endpoint){
            $data['endpoint'] = $this->_get_request_string();
        }
        return $data;
    }

    public function send_404() {
        global $wp_query;
        $wp_query->set_404();
        header('Content-Type: text/html');
        status_header(404);
        get_template_part(404);
        exit();
    }

    private function _get_request_string() {
        $request = '/' . implode('/', $this->request) . '/';
        return get_bloginfo('url') . $request;
    }

}
