<?php

class Header_Share_Functions {

    static function get_instance() {
        return new self;
    }

    /**
     * adding actions and hooks
     */
    public function add_actions() {
        add_action('after_setup_theme', array($this, 'route'));
    }

    public function route() {
        $request = isset($_REQUEST['q']) ? substr($_REQUEST['q'], 1) : trim($_SERVER['REQUEST_URI'], '/');
        if ($request) {
            $router = new Router($request);
            $router->route();
        }
    }
}

Header_Share_Functions::get_instance()->add_actions();
