<?php

class Router {

    private $_request;

    public function __construct($request) {
        $r = explode('/', preg_replace('/\?.*$/', '', $request));
        foreach ($r as $key => $segment) {
            if ($segment === '') {
                unset($r[$key]);
            }
        }
        $this->_request = array_values($r);
    }

    public function route() {
        if ($this->_is_valid_request()) {
            $this->_send_response();
            exit;
        }
        return;
    }

    private function _send_header($status = '200') {
        header("HTTP/1.1 $status");
        switch ($this->_get_request_type()) {
            case 'api':
                header('Content-Type: application/json');
                break;
            case 'css':
                header("Content-Type: text/css");
                header("X-Content-Type-Options: nosniff");
                break;
            case 'js':
                header("Content-Type: text/javascript");
                header("X-Content-Type-Options: nosniff");
                break;
            case 'woff':
                header("Content-Type: application/font-woff");
                break;
            case 'woff2':
                header("Content-Type: application/font-woff2");
                break;
            case 'ttf':
                header("Content-Type: application/font-ttf");
                break;
            case 'eot':
                header("Content-Type: application/vnd.ms-fontobject");
                break;
            case 'svg':
                header("Content-Type: image/svg+xml");
                break;
            case 'img':
                $imgs = array('jpg', 'jpeg', 'png', 'gif');
                $img = explode('.', $this->_request[count($this->_request) - 1]);
                $mime = $img[count($img) - 1];
                if (in_array($mime, $imgs)) {
                    header("Content-Type: image/$mime");
                }
                break;
            default:
                header("Content-Type: text/html");
        }
    }

    private function _send_response() {
        if ($this->_get_request_type() === 'api') {
            echo json_encode((array) $this->_build_response());
        } else {
            $this->_send_file();
        }
    }

    private function _send_file() {
        $file = $this->_get_file_location();
        if (file_exists($file)) {
            $vdh_options = get_option('vdh_theme_options');
            $ckan_instance_url = (isset($vdh_options['ckan_url']) ?
                            substr($vdh_options['ckan_url'], 0, -1) :
                            '*');
            header("Access-Control-Allow-Methods: GET");
            header("Access-Control-Allow-Origin: " . $ckan_instance_url);
            $this->_send_header();
            header("Content-Length: " . filesize($file));
            echo file_get_contents($file);
            exit;
        }
        $this->_send_header('404 - Not Found');
    }

    private function _get_file_location() {
        return get_template_directory() . '/' . implode('/', $this->_request);
    }

    private function _get_request_type() {
        $request_type = '';
        if (in_array($this->_request[0], array('api', 'js', 'img'))) {
            $request_type = $this->_request[0];
        } elseif ($this->_request[0] === 'css') {
            $res = explode('.', $this->_request[count($this->_request) - 1]);
            if (in_array($res[count($res) - 1], array('css', 'woff', 'woff2', 'ttf', 'eot', 'svg'))) {
                $request_type = $res[count($res) - 1];
            }
        }
        return $request_type;
    }

    private function _is_valid_request() {
        return count($this->_request) > 1 &&
                $this->_get_request_type();
    }

    private function _build_response() {
        $resp_class = 'Api_' . ucfirst($this->_request[1]);
        $resp_file = API_BASEPATH . '/classes/' . $resp_class . '.php';

        if (!file_exists($resp_file)) {
            $this->_send_header('404 - Not Found');
            return array('msg' => 'Wrong Call');
        }
        require_once $resp_file;

        $resp = new $resp_class($this->_request);

        $this->_send_header();

        return (array) $resp->build_response();
    }

}
