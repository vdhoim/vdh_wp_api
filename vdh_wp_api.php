<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);


/*
  Plugin Name: VDH API
  Plugin URI: http://vdh.virginia.gov
  Description:
  Version: 0.1
  Author: Alex Siaba
  Author Email: alex.siaba@vdh.virginia.gov
  License:

  Copyright 2014 Alex Siaba (alex.siaba@gmail.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 */


/*
  FUNCTIONS - overridden function from the wp-core
 */

define('API_BASEPATH', dirname(__FILE__));

require_once 'functions.php';
require_once 'router.php';

function pre_dump($var){
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}